# -*- coding: utf-8 -*-
"""
This file contains the setup of the neuronal network running the iCub experiment in a VOR task
"""
from __future__ import print_function
from __future__ import division
from builtins import range
from past.utils import old_div
# pragma: no cover


__author__ = 'Francisco Naveros, Jesus Garrido, Niceto Luque, Eduardo Ros'

### The following can be removed when PyNN 0.8 has been established or we have a more elegant
### solution
import nest
import numpy as np
import logging

import time

from pyNN.random import RandomDistribution, NumpyRNG

logger = logging.getLogger(__name__)

# Synapsis parameters
gc_pc_weights = 5.0
mf_vn_weights = 1.0
pc_vn_weights = 20.0 #NOTE: applied a factor of 1e06 wrt pynn version to this weight.
io_pc_weights = 0.0
mf_gc_weights = 0.6
go_gc_weights = -0.2
input_weights = 0.25
mf_go_weights = 0.6


# Network parameters
num_MF_neurons = 100
num_GC_neurons = 2000
num_GOC_neurons = 100
num_PC_neurons = 200
num_VN_neurons = 200
num_IO_neurons = 200

nest.SetKernelStatus({'resolution':0.5, 'min_delay':0.5, 'max_delay':100.0})

def create_brain():
	"""
	Initializes PyNN with the neuronal network that has to be simulated for the experiment
	"""

	s_start = time.time()

	GR_PARAMS = {'C_m': 2.0,
                 'E_L': -70.0,
                 'g_L': 2.0/100., #'tau_minus': 100.0,
                 'E_ex': 0.0,
                 'E_in': -75.0,
                 'V_reset': -70.0,
                 'V_th': -40.0,
				 't_ref': 1.0,
                 'tau_syn_ex': 0.5,
                 'tau_syn_in': 2.0,
				 'V_m': -70.0}

	GO_PARAMS = {'C_m': 2.0,
                 'E_L': -70.0,
                 'g_L': 2.0/100., #'tau_minus': 100.0,
                 'E_ex': 0.0,
                 'E_in': -75.0,
                 'V_reset': -70.0,
                 'V_th': -40.0,
				 't_ref': 1.0,
                 'tau_syn_ex': 0.5,
                 'tau_syn_in': 2.0,
				 'V_m': -70.0}

	PC_PARAMS = {'C_m': 314.0,
                 'g_L': 12.0,
                 'E_L': -70.0,
                 'E_ex': 0.0,
                 'E_in': -75.0,
                 'e_cs': 0.0,
                 'V_reset': -70.0,
                 'V_th': -52.0,
                 't_ref': 1.0,
                 'tau_syn_ex': 0.85,
                 'tau_syn_in': 5.45,
                 'tau_syn_cs': 0.85,
				 'V_m': -70.0}

	VN_PARAMS = {'C_m': 2.0,
                 'g_L': 0.2,
                 'E_L': -70.0,
                 'E_ex': 0.0,
                 'E_in': -80.0,
                 'e_ts': 0.0,
                 'V_reset': -70.5,
                 'V_th': -40.0,
                 't_ref': 1.0,
                 'tau_syn_ex': 0.5,
                 'tau_syn_in': 7.0,
                 'tau_syn_ts': 0.85,
                 'tau_cos': 10.0,
                 'exponent': 2.0,
				 'V_m': -70.5}

	##THIS MODULE CAN BE DOWNLOADED FROM https://github.com/jgarridoalcazar/SpikingCerebellum/
	#try:
	#	nest.Install('cerebellummodule')
	#except nest.NESTError:
	#	pass


	# Create MF population
	MF_population = nest.Create('parrot_neuron', num_MF_neurons, params = {})

	# Create GOC population
	GOC_population = nest.Create('iaf_cond_alpha', num_GOC_neurons, params = GO_PARAMS)

	print('created MF and GOC populations: {s}'.format(s=time.time()-s_start))

	# Create MF-GO connections
	mf_go_connections = nest.Connect(MF_population, GOC_population, 'one_to_one', {"model": "static_synapse", "weight":mf_go_weights, "delay":1.0})

	print('created MF-GO connections: {s}'.format(s=time.time()-s_start))


	# Create GrC population
	GC_population = nest.Create('iaf_cond_alpha', num_GC_neurons, params = GR_PARAMS)

	print('created GrC populations: {s}'.format(s=time.time()-s_start))

	# Random distribution for synapses delays and weights
	delay_distr = {'distribution': 'uniform', 'low': 1.0, 'high': 10.0}
	weight_distr_MF = {'distribution': 'uniform', 'low': mf_gc_weights*0.8, 'high': mf_gc_weights*1.2}
	weight_distr_GO = {'distribution': 'uniform', 'low': go_gc_weights*1.2, 'high': go_gc_weights*0.8}


	# Create MF-GC and GO-GC connections
	float_num_MF_neurons = float (num_MF_neurons)
	for i in range (num_MF_neurons):
		GC_medium_index = int(round((old_div(i, float_num_MF_neurons)) * num_GC_neurons))
		GC_lower_index = GC_medium_index - 40
		GC_upper_index = GC_medium_index + 60
		if(GC_lower_index < 0):
			GC_lower_index = 0

		elif(GC_upper_index > num_GC_neurons):
			GC_upper_index = num_GC_neurons

		if(GC_lower_index < GC_medium_index):
			GO_GC_con1 = nest.Connect(GOC_population[i: i+1],
                                      GC_population[GC_lower_index: GC_medium_index],
                                      'all_to_all', {"model": "static_synapse", "weight":weight_distr_GO, "delay":delay_distr})

			MF_GC_con2 = nest.Connect(MF_population[i: i+1],
                                      GC_population[GC_medium_index: GC_medium_index + 20],
                                      'all_to_all', {"model": "static_synapse", "weight":weight_distr_MF, "delay":delay_distr})

		if((GC_medium_index + 20) < GC_upper_index):
			GO_GC_con3 = nest.Connect(GOC_population[i: i+1],
                                      GC_population[GC_medium_index + 20: GC_upper_index],
                                      'all_to_all', {"model": "static_synapse", "weight":weight_distr_GO, "delay":delay_distr})


	print('created MF-GC and GO-GC connections: {s}'.format(s=time.time()-s_start))


	# Create PC population (THIS MODEL HAS BEEN DEFINED IN THE CEREBELLUMMODULE PACKAGE: https://github.com/jgarridoalcazar/SpikingCerebellum/)
	PC_population = nest.Create('iaf_cond_exp_cs', num_PC_neurons, params = PC_PARAMS)

	print('created PC populations: {s}'.format(s=time.time()-s_start))

	# Create VN population (THIS MODEL HAS BEEN DEFINED IN THE CEREBELLUMMODULE PACKAGE: https://github.com/jgarridoalcazar/SpikingCerebellum/)
	VN_population = nest.Create('iaf_cond_exp_cos', num_VN_neurons, params = VN_PARAMS)

	print('created VN populations: {s}'.format(s=time.time()-s_start))

	# Create IO population
	IO_population = nest.Create('parrot_neuron', num_IO_neurons,params = {})

	print('created IO populations: {s}'.format(s=time.time()-s_start))



	# Create MF-VN learning rule (THIS MODEL HAS BEEN DEFINED IN THE CEREBELLUMMODULE PACKAGE: https://github.com/jgarridoalcazar/SpikingCerebellum/)


    	# Create MF-VN connections
	mf_vn_connections = nest.Connect(MF_population,
					VN_population,
					'all_to_all', #AMPA
					{'model':'stdp_cos_synapse','weight':mf_vn_weights, 'receptor_type': 1,
                                                              'delay':1.0,
                                                              'exponent': 2.0,
                                                              'tau_cos': 10.0,
                                                              'A_plus': 0.0009,
                                                              'A_minus': 0.01,
                                                              'Wmin': 0.5,
                                                              'Wmax': 7.0})

	print('created MF-VN connections: {s}'.format(s=time.time()-s_start))

	# Create PC-VN connections
	pc_vn_connections = nest.Connect(PC_population,
                                       VN_population,
                                       'one_to_one', #GABA
                                       {"model": "static_synapse", "weight":pc_vn_weights, 'receptor_type': 2 ,"delay":1.0})

	# This second synapse with "receptor_type=TEACHING_SIGNAL" propagates the learning signals that drive the plasticity mechanisms in MF-VN synapses
	pc_vn_connections_2 = nest.Connect(PC_population,
                                       VN_population,
                                       'one_to_one', #TEACHING_SIGNAL
                                       {"model": "static_synapse", "weight":0.0, 'receptor_type': 3 ,"delay":1.0})

	print('created PC-VN connections: {s}'.format(s=time.time()-s_start))


	# Create MF-VN learning rule (THIS MODEL HAS BEEN DEFINED IN THE CEREBELLUMMODULE PACKAGE: https://github.com/jgarridoalcazar/SpikingCerebellum/)


	# Create GC-PC connections
	gc_pc_connections = nest.Connect(GC_population,
					PC_population,
					'all_to_all',#AMPA
					{'weight':gc_pc_weights, 'model': 'stdp_sin_synapse', 'receptor_type': 1,
                                                             'delay':1.0,
                                                             'exponent': 10,
                                                             'peak': 100.0,
                                                             'A_plus': 0.014,
                                                             'A_minus': 0.08,
                                                             'Wmin': 0.000,
                                                             'Wmax': 10.0})

	print('created GC-PC connections: {s}'.format(s=time.time()-s_start))

	# Create IO-PC connections. This synapse with "receptor_type=COMPLEX_SPIKE" propagates the learning signals that drive the plasticity mechanisms in GC-PC synapses
	io_pc_connections = nest.Connect(IO_population,
                                       PC_population,
                                       'one_to_one', #COMPLEX_SPIKE
                                       {"model": "static_synapse", "weight":io_pc_weights, 'receptor_type': 3 ,"delay":1.0})

	print('created IO-PC connections: {s}'.format(s=time.time()-s_start))

	# Group all neural layers
	population = MF_population + GOC_population + GC_population + PC_population + VN_population + IO_population

	# Set Vm to resting potential
	# sim.initialize(PC_population, V_m=PC_population.get('E_L'))
	# sim.initialize(VN_population, V_m=VN_population.get('E_L'))

	print('num of cells: {s}'.format(s = len(population)))

	return population


circuit = create_brain()
