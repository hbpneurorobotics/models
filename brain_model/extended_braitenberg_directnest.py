# -*- coding: utf-8 -*-
"""
This file contains the setup of the neuronal network running the Husky experiment with neuronal image recognition
"""
from __future__ import division
from builtins import range
from past.utils import old_div
# pragma: no cover

import nest
import logging
import random

logger = logging.getLogger(__name__)

def create_brain():
    """
    Initializes NEST with the neuronal network that has to be simulated
    """

    ##
    ## Set up neurons
    ##

    INPUT_PARAMS = {'a': 4.0,
                    'b': 0.0805,
                    'Delta_T': 2.0,
                    'tau_w': 144.0,
                    'V_peak': 0.0,
                    'C_m': 281.0, # ev. /1000
                    'E_L': -70.6,
                    'g_L': 281.0/9.3666667,
                    'E_ex': 0.0,
                    'E_in': -80.0,
                    'V_reset': -70.6,
                    'V_th': -50.4,
                    't_ref': 10.0,
                    'tau_syn_ex': 5.,
                    'tau_syn_in': 5.}

    SENSORPARAMS = {'b': 0.0,
                    'tau_w': 10.0,
                    'V_peak': 0.0,
                    'C_m': 25.,
                    'E_L': -60.5,
                    'g_L': 25./10.,
                    'E_ex': 0.0,
                    'E_in': -75.0,
                    'V_reset': -60.5,
                    'V_th': -60.0,
                    't_ref': 10.0,
                    'tau_syn_ex': 2.5,
                    'tau_syn_ex': 7.5}

    GO_ON_PARAMS = {'C_m': 25.,
                    'E_L': -60.5,
                    'g_L': 25./10.,
                    'E_ex': 0.0,
                    'E_in': -75.0,
                    'V_reset': -61.6,
                    'V_th': -60.51,
                    't_ref': 10.0,
                    'tau_syn_ex': 2.5,
                    'tau_syn_ex': 7.5}

    INTERMEDIATE_PARAMS = {'a': 4.0,
                           'b': 0.0805,
                           'Delta_T': 2.0,
                           'tau_w': 144.0,
                           'V_peak': 0.0,
                           'C_m': 281.0, # ev. /1000
                           'V_reset': -70.6,
                           'g_L': 281.0/112.4,
                           'E_ex': 0.0,
                           'E_in': -80.0,
                           'V_reset': -70.6,
                           'V_th': -50.4,
                           't_ref': 10.0,
                           'tau_syn_ex': 5.,
                           'tau_syn_ex': 5.}

    population = nest.Create('aeif_cond_alpha', 709)
    nest.SetStatus(population[0:600], INPUT_PARAMS)
    nest.SetStatus(population[600:605], SENSORPARAMS)
    nest.SetStatus(population[605:606], GO_ON_PARAMS)
    nest.SetStatus(population[606:608], SENSORPARAMS)
    nest.SetStatus(population[608:708], INTERMEDIATE_PARAMS)
    nest.SetStatus(population[708:709], GO_ON_PARAMS)

    ##
    ## Set up synapse types
    ##

    SYNAPSE_PARAMS = {
        "delay": 0.1,
    }

    # Synaptic weights
    weight_red_to_actor = 15.  # was 1.5e-4
    weight_red_to_go_on = 1.0e6  # or -1.2e-3?
    weight_green_blue_to_actor = 1.0e3
    weight_go_on_to_right_actor = 1.4e-1  # was 1.4e-4
    weight_eval_red = 25.
    weight_eval_to_red_sensor = 8.75e-2  # was 1.0e-4, then 8.75e-5
    weight_eval_to_bg_sensor = 5.0e3  # was 1.5, then 10.0, change it further
    weight_red_to_blue_eval = 1.0e6  # was 5e-8
    DELAY = 0.1

    synapse_red_to_actor = {'model': 'static_synapse', 'weight': weight_red_to_actor, 'delay': DELAY}
    synapse_red_to_go_on = {'model': 'static_synapse', 'weight': -weight_red_to_go_on,'delay': DELAY}
    synapse_green_blue_to_actor = {'model': 'static_synapse', 'weight': weight_green_blue_to_actor,
                                                            'delay': DELAY}
    synapse_go_on_to_right_actor ={'model': 'static_synapse', 'weight': weight_go_on_to_right_actor,
                                                            'delay': DELAY}
    synapse_eval_red = {'model': 'static_synapse', 'weight': weight_eval_red, 'delay': DELAY}
    synapse_eval_to_red_sensor = {'model': 'static_synapse', 'weight': weight_eval_to_red_sensor,
                                                            'delay': DELAY}
    synapse_eval_to_bg_sensor = {'model': 'static_synapse', 'weight': weight_eval_to_bg_sensor,
                                                            'delay': DELAY}
    synapse_red_to_blue_eval = {'model': 'static_synapse', 'weight': -weight_red_to_blue_eval,
                                                            'delay': DELAY}

    ##
    ## Set up projections
    ##

    nest.Connect(population[602:603], population[607:608],
                   'all_to_all',
                   synapse_red_to_actor)
    nest.Connect(population[603:604], population[606:607],
                   'all_to_all',
                   synapse_red_to_actor)


    nest.Connect(population[600:602], population[605:606],
                   'all_to_all',
                   synapse_red_to_go_on)


    nest.Connect(population[604:605], population[607:608],
                   'all_to_all',
                   synapse_green_blue_to_actor)


    nest.Connect(population[605:606], population[607:608],
                   'all_to_all',
                   synapse_go_on_to_right_actor)


    # connect the left portion of the detector neurons to the left evaluator and the right portion to the right one
    pre = random.sample(list(range(300)), 300)
    for i in range(608,658):
        for j in range(6):
            nest.Connect([population[pre[j+(i-608)*6]]], [population[i]],
                        'one_to_one',
                        synapse_eval_red)

    pre = random.sample(list(range(300,600)), 300)
    for i in range(658,708):
        for j in range(6):
            nest.Connect([population[pre[j+(i-658)*6]]], [population[i]],
                        'one_to_one',
                        synapse_eval_red)

    nest.Connect(population[608:658], population[600:601],
                   'all_to_all',
                   synapse_eval_to_red_sensor)
    nest.Connect(population[608:658], population[602:603],
                   'all_to_all',
                   synapse_eval_to_red_sensor)
    nest.Connect(population[658:708], population[601:602],
                   'all_to_all',
                   synapse_eval_to_red_sensor)
    nest.Connect(population[658:708], population[603:604],
                   'all_to_all',
                   synapse_eval_to_red_sensor)


    nest.Connect(population[600:602], population[604:605],
                   'all_to_all',
                   synapse_red_to_blue_eval)

    nest.Connect(population[708:709], population[604:605],
                   'one_to_one',
                   synapse_eval_to_bg_sensor)  # connect the "blue green evaluator" to the blue green sensor

    return population

circuit = create_brain()
sensors = circuit[0:605]
actors = circuit[605:608]
evaluators = circuit[608:709]
