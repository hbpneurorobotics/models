# -*- coding: utf-8 -*-
"""
This file contains the setup of the neuronal network running the Husky experiment with neuronal image recognition
"""
# pragma: no cover

__author__ = 'Lazar Mateev, Georg Hinkel'

from hbp_nrp_cle.brainsim import simulator as sim
import numpy as np
import logging

logger = logging.getLogger(__name__)


def create_brain():
    """
    Initializes PyNN with the neuronal network that has to be simulated
    """
    SENSORPARAMS = {'v_rest': -60.5,
                    'cm': 0.025,
                    'tau_m': 10.,
                    'tau_refrac': 10.0,
                    'tau_syn_E': 2.5,
                    'tau_syn_I': 2.5,
                    'e_rev_E': 0.0,
                    'e_rev_I': -75.0,
                    'v_thresh': -60.0,
                    'v_reset': -60.5}

    GO_ON_PARAMS = {'v_rest': -60.5,
                    'cm': 0.025,
                    'tau_m': 10.0,
                    'e_rev_E': 0.0,
                    'e_rev_I': -75.0,
                    'v_reset': -61.6,
                    'v_thresh': -60.51,
                    'tau_refrac': 10.0,
                    'tau_syn_E': 2.5,
                    'tau_syn_I': 2.5}

    # POPULATION_PARAMS = SENSORPARAMS * 5 + GO_ON_PARAMS + SENSORPARAMS * 2
    red_left_eye = sim.Population(2, sim.IF_cond_exp(**SENSORPARAMS), label="red_left_eye")
    red_right_eye = sim.Population(2, sim.IF_cond_exp(**SENSORPARAMS), label="red_right_eye")
    green_blue_eye = sim.Population(1, sim.IF_cond_exp(**SENSORPARAMS), label="green_blue_eye")
    go_on = sim.Population(1, sim.IF_cond_exp(**GO_ON_PARAMS), label="go_on")
    left_wheel_motor = sim.Population(1, sim.IF_cond_exp(**SENSORPARAMS), label="left_wheel_motor")
    right_wheel_motor = sim.Population(1, sim.IF_cond_exp(**SENSORPARAMS), label="right_wheel_motor")

    # population = sim.Population(8, sim.IF_cond_exp())
    # population[0:5].set(**SENSORPARAMS) # 0, 2 = red_left_eye, 1, 3 = red_right_eye, 4 = green_blue_eye
    # population[5:6].set(**GO_ON_PARAMS) # 5 = go_on?
    # population[6:8].set(**SENSORPARAMS) # 6 = left_wheel_motor, 7=right_wheel_motor

    # Shared Synapse Parameters
    # syn_params = {'U': 1.0, 'tau_rec': 1.0, 'tau_facil': 1.0}

    # Synaptic weights
    WEIGHT_RED_TO_ACTOR = 1.5e-4
    WEIGHT_RED_TO_GO_ON = 1.2e-3  # or -1.2e-3?
    WEIGHT_GREEN_BLUE_TO_ACTOR = 1.05e-4
    WEIGHT_GO_ON_TO_RIGHT_ACTOR = 1.4e-4
    DELAY = 0.1

    # Connect neurons

    SYN = sim.StaticSynapse(weight=abs(WEIGHT_RED_TO_ACTOR), delay=DELAY)
    sim.Projection(red_left_eye, # red_left_eye[1]
                   right_wheel_motor, # right_wheel_motor
                   connector=sim.FromListConnector([(1, 0)]),
                   synapse_type=SYN,
                   receptor_type='excitatory')
    sim.Projection(red_right_eye, # red_right_eye[1]
                   left_wheel_motor, # left_wheel_motor
                   connector=sim.FromListConnector([(1, 0)]),
                   synapse_type=SYN,
                   receptor_type='excitatory')

    SYN = sim.StaticSynapse(weight=abs(WEIGHT_RED_TO_GO_ON), delay=DELAY)
    sim.Projection(red_left_eye, # red_left_eye
                   green_blue_eye, # green_blue_eye
                   connector=sim.AllToAllConnector(),
                   synapse_type=SYN,
                   receptor_type='inhibitory')
    sim.Projection(red_left_eye, # red_left_eye
                   go_on, # go_on
                   connector=sim.AllToAllConnector(),
                   synapse_type=SYN,
                   receptor_type='inhibitory')

    SYN = sim.StaticSynapse(weight=abs(WEIGHT_GREEN_BLUE_TO_ACTOR), delay=DELAY)
    sim.Projection(green_blue_eye, # green_blue_eye
                   right_wheel_motor, # right_wheel_motor
                   connector=sim.AllToAllConnector(),
                   synapse_type=SYN,
                   receptor_type='excitatory')

    SYN = sim.StaticSynapse(weight=abs(WEIGHT_GO_ON_TO_RIGHT_ACTOR), delay=DELAY)
    sim.Projection(go_on, # go_on
                   right_wheel_motor, # right_wheel_motor
                   connector=sim.AllToAllConnector(),
                   synapse_type=SYN,
                   receptor_type='excitatory')

    # sim.initialize(population, v=population.get('v_rest'))

    # logger.debug("Circuit description: " + str(population.describe()))

    return {"red_left_eye": red_left_eye, "red_right_eye": red_right_eye,
            "green_blue_eye": green_blue_eye, "go_on": go_on,
            "left_wheel_motor": left_wheel_motor, "right_wheel_motor": right_wheel_motor}


circuit = create_brain()
