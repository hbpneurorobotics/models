"""
A controller for the iCub visual tracking experiment
"""
from __future__ import division
from builtins import object
from past.utils import old_div

__author__ = 'AlessandroAmbrosano'

import threading
import cv2
import cv_bridge
import rospy
import numpy as np
from std_msgs.msg import Float64
from geometry_msgs.msg import Point
import time
from gazebo_msgs.msg import ModelState
from sensor_msgs.msg import JointState, Image


class Controller(object):
    """
    Controller class.
    Define the callback functions and binds them to topics.
    """
    def __init__(self):
        rospy.init_node('node')
        rospy.Subscriber('/tf_results', Point, self.on_results)
        self.joint_states = rospy.Publisher('/joint_states', JointState)
        rospy.Subscriber('/robot/joints', JointState, self.on_joints)
        rospy.Subscriber('/icub_model/left_eye_camera/image_raw', Image, self.on_image)

        self.left_eye = rospy.Publisher('/robot/left_eye_pan/pos', Float64)
        self.right_eye = rospy.Publisher('/robot/right_eye_pan/pos', Float64)
        self.model_state = rospy.Publisher('/gazebo/set_model_state', ModelState)

        self.time = 0
        self.eyepos = None
        self.can_go = False

    @staticmethod
    def rad2deg(rad):
        """
        Radians to degrees conversion function.
        :param rad: value in radians
        :return: value of rad in degrees
        """
        return (old_div(float(rad), (2. * np.pi))) * 360

    @staticmethod
    def deg2rad(deg):
        """
        Degrees to radians conversion function.
        :param deg: value in degrees
        :return: value of deg in radians
        """
        return (old_div(float(deg), 360.)) * (2. * np.pi)

    def on_results(self, results):
        """
        Transfer function update event callback
        """
        if self.eyepos is None or self.can_go is False:
            return
        r = results.y
        l = results.x
        # print "l: %f, r: %f" % (l, r)
        d = r - l
        max_mov = 1.0
        self.eyepos += Controller.deg2rad(-((d + 0.03) / 0.09 * 2 * max_mov - max_mov))
        self.left_eye.publish(self.eyepos)
        self.right_eye.publish(self.eyepos)

        self.target_mover(0.3, 0.3)

    def on_joints(self, joints):
        """
        Robot Joints update event callback
        :param joints: New joint state information
        """
        self.eyepos = joints.position[joints.name.index('left_eye_pan')]
        l = ['eye_version']
        js = JointState()
        js.header = joints.header
        js.name = l
        js.position = [joints.position[joints.name.index(x)] for x in l]
        js.velocity = [joints.velocity[joints.name.index(x)] for x in l]
        js.effort = [joints.effort[joints.name.index(x)] for x in l]
        # print js
        self.joint_states.publish(js)

    def on_image(self, image):
        """
        New image event callback
        :param image: the new image
        :return:
        """
        if image is not None:
            self.can_go = True

    def target_mover(self, f, a):
        """
        Move the target in a sinusoidal fashion,
        setting its ModelState using Gazebo API.
        :param f: The frequency of the movement
        :param a: The amplitude of the movement
        """

        m = ModelState()
        frequency = f
        amplitude = a
        t = self.time
        m.model_name = 'Target'
        
        # set orientation RYP axes
        m.pose.orientation.x = 0
        m.pose.orientation.y = 1
        m.pose.orientation.z = 1

        target_center = {'x': 0, 'y': 2.42, 'z': 1.2}
        m.reference_frame = 'world'
        m.pose.position.x = target_center['x'] + np.sin(t * frequency * 2 * np.pi) * (old_div(float(amplitude), 2))
        m.pose.position.y = target_center['y']
        m.pose.position.z = target_center['z']
        self.model_state.publish(m)
        self.time += 0.02

if __name__ == '__main__':
    c = Controller()
    threading.Thread(target=rospy.spin).start()
