# MMM model port to gazebo and NRP
# Authors: J. Camilo Vasquez Tieck, Tristan Schnell

## Contents

mmm_robot.urdf / mmm_robot.sdf: regular version of MMM model with enabled collision that stands balanced

mmm_robot_anchored.urdf / mmm_robot_anchored.sdf: anchored model with disabled collisions that is used to play motions

publishmotion.py: allows publishing of a motion or single motion frames


## Installation
For usage with Gazebo:
 - Clone into catkin workspace
 - catkin_make

 For usage with the NRP:
 - Clone into Models folder
 - Rerun create_symlinks and deploy


## Usage
Gazebo:
1. Import/spawn mmm_robot.urdf in Gazebo.
 - E.g. export GAZEBO_MODEL_PATH=~/catkin_ws/src/

2. Download a motion from KIT Whole-Body Motion Database (or use the included motion)

3. rosrun mmm_motion publishmotion.py
 - run with --help for list of options
 - for defaults to work, run from folder containing walk_neutral02.xml


NRP:
Only used as ressource for the MMM experiments


## References
https://gitlab.com/mastermotormap/mmmcore
https://gitlab.com/mastermotormap/mmmtools
https://motion-database.humanoids.kit.edu/